# Pet shop - Telerik Alpha 23 Task
## Important
- Before you start please CREATE A NEW BRANCH FROM MASTER AND NAME IT AFTER YOUR NAME. Work only there and DON`T PUSH ANYTHING TO MASTER.
You rock!

## **Tasks:**

## Models
- Implement all interfaces to the corresponding classes and add the followint validations:

**Animal**
- AnimalType cannot be null. It is mandatory when crreating a new animal.
- Breed is optional, not mandatory when creating a new animal.
- Name of the animal is mandatory when creating a new animal

**Customer**
- Name is mandatory when creating a new customer. Name cannot be null.
- The list of animals can be empty and is not mandatory when creating a new customer. HINT: you must make sure the list is initialized.

**IFood interface implementation task**
- This interface must be implemented by the Food class. Before that though you need to add the following to the interface:
- Properties: string Name, decimal Price

**Food**
- Name and Price are mandatory when creating a new food. All those properties cannot be null, the Price cannot be a negative number
 
## Core 
**Database**
- In this class you have lists of Animals, Customers and Food. There are couple of methods you need to implement here. 
- Hint: TODO included

**Factory**
- Here happens the creation, initializing of the new instances. You must implement all three methods.
- Hint: TODO included
- IMPORTANT: Remember to look for the order of the parameters you are receiving!

**Engine**
- Here everything is set.I have included some comments, so it is easier for you to understand the concept of this class

**Hints**
- Look for TODO-s within the solution to find explanations and tasks you need to finish.
- The TODO list can be pined to your VS by clicking on the ribbon -> View -> Task list.