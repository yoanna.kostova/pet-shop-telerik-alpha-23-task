﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetShop.Models.Contracts
{
    public interface IFood
    {
        public string Name { get; set; }
        public string Print();
    }
}
