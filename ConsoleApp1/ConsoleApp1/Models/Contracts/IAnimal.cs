﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace PetShop.Models.Contracts
{
    public interface IAnimal
    {
        string AnimalName { get; set; }
        public string AnimalType { get; set; }
        public string Breed { get; set; }
        public string Print();

    }
}
