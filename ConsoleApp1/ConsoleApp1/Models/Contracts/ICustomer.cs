﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetShop.Models.Contracts
{
    public interface ICustomer
    {
        public string Name { get; set; }
        public List<Animal> Animals { get; set; }
        public string Print();

    }
}
