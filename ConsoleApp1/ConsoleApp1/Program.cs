﻿using PetShop.Core;
using System;

namespace PetShop

{
    class Program
    {
            public static void Main() => Engine.Instance.Run();
    }
}
