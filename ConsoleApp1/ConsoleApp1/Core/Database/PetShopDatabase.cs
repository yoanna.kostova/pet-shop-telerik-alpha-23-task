﻿using PetShop.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PetShop.Core.Database
{
    public class PetShopDatabase
    {
        //TODO: Implement all methods 

        private List<Animal> animals;
        private List<Customer> customers;
        private List<Food> foods;

        public PetShopDatabase()
        {
            this.Animals = new List<Animal>();
            this.Customers = new List<Customer>();
            this.Foods = new List<Food>();

            this.Animals = animals;
            this.Customers = customers;
            this.Foods = foods;
        }

        public List<Food> Foods { get => this.foods; set => this.foods = value; }
        public List<Customer> Customers { get => this.customers; set => this.customers = value; }
        public List<Animal> Animals { get => this.animals; set => this.animals = value; }

        public void AddAnimalToDatabase(Animal animal)
        {
            throw new NotImplementedException();
        }
        public void AddCustomerToDatabase(Customer customer)
        {
            throw new NotImplementedException();
        }
        public void AddFoodToDatabase(Food food)
        {
            throw new NotImplementedException();
        }

        public void RemoveAnimalFromDatabase(Animal animal)
        {
            throw new NotImplementedException();
        }
        public void RemoveCustomerFromDatabase(Customer customer)
        {
            throw new NotImplementedException();
        }
        public void RemoveFoodFromDatabase(Food food)
        {
            throw new NotImplementedException();
        }
    }
}
