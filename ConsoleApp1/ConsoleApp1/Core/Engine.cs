﻿using PetShop.Core.Contracts;
using PetShop.Core.Factory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace PetShop.Core
{
    public class Engine
    {
        private const string Delimiter = "---------------------";
        private readonly PetShopFactory factory = new PetShopFactory();

        //This is a static constructor. It is called only once during the whole life of the application. The reason we do dtaht is to make sure that the engine we are working with is only one.

        //This is a design pattern called Singleton. Using this design pattern we do exacly as I mentioned - we make sure we have a single instance of what we initiate in the static constructor.
        static Engine()
        {
            Instance = new Engine();
        }

        public void Run()
        {
            while (true)
            {
                Console.WriteLine("Write Exit to exit the application");
                Console.WriteLine("To create a new Customer write CreateCustomer");
                Console.WriteLine("To create a new Animal write CreateAnimal");
                Console.WriteLine("To create a new Food write CreateFood");
                Console.WriteLine("After every command, press Enter");
                string input = Console.ReadLine();
                string result = this.Process(input);
                this.Print(result);
            }
        }

        public static Engine Instance { get; }


        private string Process(string input)
        {
            List<string> inputParameters = new List<string>();

            if (input == "Exit")
                Environment.Exit(0);

            //Because of this try-catch block, when an exception is thrown, the application does not stop working.

            try
            {
                switch (input)
                {
                    case "CreateCustomer":
                        Console.WriteLine("Write customer name and press enter:");
                        inputParameters.Add(Console.ReadLine());
                        this.factory.CreateCustomer(inputParameters[0]);
                        inputParameters.Clear();
                        break;

                    case "CreateAnimal":
                        Console.WriteLine("Write animal name and press enter:");
                        inputParameters.Add(Console.ReadLine());
                        Console.WriteLine("Write animal type and press enter:");
                        inputParameters.Add(Console.ReadLine());
                        Console.WriteLine("Write animal breed and press enter:");
                        inputParameters.Add(Console.ReadLine());
                        this.factory.CreateAnimal(inputParameters[0], inputParameters[1], inputParameters[2]);
                        inputParameters.Clear();
                        break;

                    case "CreateFood":
                        Console.WriteLine("Write the name of the food and press enter:");
                        inputParameters.Add(Console.ReadLine());
                        this.factory.CreateFood(inputParameters[0]);
                        inputParameters.Clear();
                        break;

                    default:
                        return "Please choose a valid command";
                }
                return "Command successfully executed";
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                return $"ERROR: {e.Message}";
            }
        }

        private void Print(string message)
        {
            Console.WriteLine(message);
        }
    }
}
