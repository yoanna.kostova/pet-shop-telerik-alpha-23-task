﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetShop.Core.Contracts
{
    public interface IPetShopFactory
    {
        string CreateAnimal(string name, string animalType, string breed);
        string CreateCustomer(string name);
        string CreateFood(string name);
    }
}
